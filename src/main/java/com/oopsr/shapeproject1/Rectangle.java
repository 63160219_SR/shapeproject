/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oopsr.shapeproject1;

/**
 *
 * @author USER
 */
public class Rectangle {
    private double h;
    private double w;
    public Rectangle(double h,double w){
       this.h = h;
       this.w = w;
    }
    public double calArea(){
        return h*w;
    }
    public void sethw(double h,double w) {
        if (h <= 0){
            System.out.println("Error: hight must more than zero!");
            return;
        }
        else if (w <= 0){
            System.out.println("Error: wide must more than zero!");
            return;
        }
        this.h = h;
        this.w = w;
    }
}
