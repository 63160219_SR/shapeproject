/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oopsr.shapeproject1;

/**
 *
 * @author USER
 */
public class Square {
    private double s;
    public Square(double s){
       this.s = s;
    }
    public double calArea(){
        return s*s;
    }
    public void seth(double s) {
        if (s <= 0){
            System.out.println("Error: num must more than zero!");
            return;
        }
        this.s = s;
    }
}
