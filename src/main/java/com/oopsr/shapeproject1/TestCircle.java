/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oopsr.shapeproject1;

/**
 *
 * @author USER
 */
public class TestCircle {
    public static void main(String[] args) {
        Circle circle1 = new Circle(9);
        System.out.println("Area of Circle("+ circle1.getR()+") = " + circle1.calArea());
        circle1.setR(2);
        System.out.println("Area of Circle("+ circle1.getR()+") = " + circle1.calArea());
        circle1.setR(0);
        System.out.println("Area of Circle("+ circle1.getR()+") = " + circle1.calArea());
    }
}
