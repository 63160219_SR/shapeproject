/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oopsr.shapeproject1;

/**
 *
 * @author USER
 */
public class Triangle {
    private double h;
    private double b;
    public Triangle(double h,double b){
       this.h = h;
       this.b = b;
    }
    public double calArea(){
        return 0.5*b*h;
    }
    public void sethb(double h,double b) {
        if (h <= 0){
            System.out.println("Error: hight must more than zero!");
            return;
        }
        else if (b <= 0){
            System.out.println("Error: base must more than zero!");
            return;
        }
        this.h = h;
        this.b = b;
    }
}
